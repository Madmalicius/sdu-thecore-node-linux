# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Logging via [winston](https://github.com/winstonjs/winston)
- Environment variables via `.env`
- Code formatting with [Prettier](https://prettier.io)
- Linting with [ESLint](https://eslint.org/)
- Testing via [Jest](https://jestjs.io/)
- Mocking with `babel-plugin-rewire` and `babel-jest`
- Status polling of remote REST API
- [MIT License](https://mit-license.org/)
- CI with `.gitlab-ci.yml`
- Automatic tagging via [CHANGELOG.md](./CHANGELOG.md)
