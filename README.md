# SDU - The Core - Node Linux

A node to manage a device of the manufacturing platform "The Core".

## Installation

Start out by creating a `.env` file in the project root with the following content:

```ini
API_URL=http://localhost:9000
POLL_INTERVAL=10000
```

Make sure to have at least the latest LTS of [nodejs](https://nodejs.org/en/) installed, then run:

```shell
$ npm i
```

Once you have the dependencies installed, you can run:

```shell
$ npm start
```

## Linting

In order to verify the code, you can run [ESLint](https://eslint.org) by typing:

```shell
$ npm run lint
```

## Formatting

If you are not using a plugin for your IDE to automatically format the code with [Prettier](https://prettier.io), you can run it via:

```shell
$ npm run format
```

## Testing

To run the unit test suite of this application, run:

```shell
$ npm run test:unit
```

## License

This project is licensed under the terms of the [MIT License](https://mit-license.org).
