const stripAnsi = require('strip-ansi');
const logger = require('./logger');

// removes color codes and tabs to make output comparable
const sanitize = string => stripAnsi(string).replace(/\t/g, '');

describe('logger', () => {
  it('exposes the winston interface', () => {
    // assert
    expect(logger.error).toBeInstanceOf(Function);
    expect(logger.warn).toBeInstanceOf(Function);
    expect(logger.info).toBeInstanceOf(Function);
    expect(logger.verbose).toBeInstanceOf(Function);
    expect(logger.debug).toBeInstanceOf(Function);
    expect(logger.silly).toBeInstanceOf(Function);
  });

  describe('formatText', () => {
    it('generates a human readable message and formats objects to JSON', () => {
      // arrange
      const formatText = logger.__GetDependency__('formatText');
      const info = {
        a: 'test!',
        level: '\u001b[32minfo\u001b[39m',
        message: '\tThis is',
        timestamp: '2018-09-20T21:34:56.280Z',
        [Symbol('level')]: 'info',
        [Symbol('splat')]: [{ a: 'test!' }],
        [Symbol('message')]: '{"a":"test!","level":"info","message":"This is"}'
      };
      const logline = '2018-09-20 21:34:56 info: This is {"a":"test!"}';

      // act
      const message = sanitize(formatText(info));

      // assert
      expect(message).toBe(logline);
    });

    it('generates a human readable message and omits supplementary strings', () => {
      // arrange
      const formatText = logger.__GetDependency__('formatText');
      const info = {
        level: '\u001b[32minfo\u001b[39m',
        message: '\tThis is a test!',
        timestamp: '2018-09-20T21:34:56.280Z',
        [Symbol('level')]: 'info',
        [Symbol('splat')]: ['Does not', 'show up!'],
        [Symbol('message')]: '{"level":"info","message":"This is a test!"}'
      };
      const logline = '2018-09-20 21:34:56 info: This is a test! ';

      // act
      const message = sanitize(formatText(info));

      // assert
      expect(message).toBe(logline);
    });
  });
});
