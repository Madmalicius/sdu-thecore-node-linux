const { createLogger, transports, format } = require('winston');

function formatText(info) {
  const { timestamp, level, message, ...args } = info;
  const ts = timestamp.slice(0, 19).replace('T', ' ');
  const rest = Object.keys(args).length ? JSON.stringify(args, '', '') : '';
  return `${ts} ${level}: ${message} ${rest}`;
}

module.exports = createLogger({
  transports: [
    new transports.Console({
      format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.align(),
        format.printf(formatText)
      )
    })
  ]
});
