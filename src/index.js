const axios = require('axios');
const { config } = require('dotenv');
const logger = require('./common/logger');

const dotenv = config();

if (dotenv.error) {
  throw dotenv.error;
}

const client = axios.create({
  baseURL: `${process.env.API_URL}/v1`
});

function displayUptime(health) {
  const start = new Date(health.started_at).getTime();
  const stop = new Date().getTime();
  const diff = stop - start;
  const days = Math.floor(diff / (1000 * 60 * 60 * 24));
  const hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((diff % (1000 * 60)) / 1000);
  logger.info(`Server up since: ${days}d ${hours}h ${minutes}min ${seconds}s`);
}

async function loop() {
  const result = await client.get('/health');
  displayUptime(result.data);
}

loop();
setInterval(loop, parseFloat(process.env.POLL_INTERVAL));
